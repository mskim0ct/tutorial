package com.fastcampus.chapter10.decorator;

@FunctionalInterface
public interface PriceProcessor {

    //실제 처리하는 로직을 포함할 메서드
    Price process(Price price);

    //기능 추가 메소드 - 해당 메소드를 통해, 다양한 기능을 연결
    default PriceProcessor andThen(PriceProcessor next){
        return price -> next.process(process(price));
    }
}
