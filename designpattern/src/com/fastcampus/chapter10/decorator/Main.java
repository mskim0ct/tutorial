package com.fastcampus.chapter10.decorator;

/***
 * The Red: 25개 백엔드 개발 필수 현업 예제를 통해 마스터하는 JAVA STREAM
 * **/

public class Main {
    public static void main(String[] args) {

        BasicPriceProcessor basicPriceProcessor = new BasicPriceProcessor();
        DiscountPriceProcessor discountPriceProcessor = new DiscountPriceProcessor();
        TaxPriceProcessor taxPriceProcessor = new TaxPriceProcessor();

        //decorate 를 적용하여, 처리할 다양한 기능들을 연결
        PriceProcessor priceProcessor = basicPriceProcessor
                .andThen(discountPriceProcessor)
                .andThen(taxPriceProcessor);


        Price unprocessedPrice = new Price("Original Price");
        //처리할 객체 넣고 처리 시작
        Price processedPrice = priceProcessor.process(unprocessedPrice);

        //결과
        System.out.println(processedPrice.getPrice());


        //////////////////////////
        //기능이 많아질수록 생성해야할 클래스가 많아짐.
        //람다를 이용한 함수형 프로그래밍으로 작성
        // -> 추가 기능 구현을 람다를 이용하여 작성하므로써, 클래스 정의 및 생성을 따로 하지 않아도 됨.
        // 즉, 기능을 바로 추가할 수 있음
        // 단, 기능들을 재사용할 가능성이 있는 경우, 클래스를 정의하고 생성하여 사용하는 것이 나음. 왜냐면 람다는 일회용성이기 때문에...


        PriceProcessor priceProcessor2 = basicPriceProcessor
                .andThen(taxPriceProcessor)
                .andThen(price ->       //<= 람다를 이용하여, 클래스 정의 및 생성 없이, 처리 기능 구현
                        new Price(price.getPrice() + ", then apply another procedure"));

        Price processedPrice2 = priceProcessor2.process(unprocessedPrice);
        System.out.println(processedPrice2.getPrice());
    }
}
