package com.fastcampus.chapter10.chanofresponsibility;

import java.util.Optional;
import java.util.function.Consumer;

public class OrderProcessStep {

    private final Consumer<Order> processOrder;
    private OrderProcessStep next;

    public OrderProcessStep(Consumer<Order> processOrder){
        this.processOrder = processOrder;
    }

    //다음 단계를  지정해주는 함수
    public OrderProcessStep setNext(OrderProcessStep next){
        if ( this.next == null){
            this.next = next;
        }else{
            this.next.setNext(next);
        }
        return this;
    }

    //내가 처리해야 할 작업
    public void process(Order order){
        processOrder.accept(order);
        Optional.ofNullable(next)
                .ifPresent(nextStep -> nextStep.process(order));
    }
}
