package com.fastcampus.chapter10.templateMethod;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class UserServiceInFunctionalWay {
    private final Predicate<User> validateUser; // <- detail이 됨
    private final Consumer<User> writeToDB; // <- detail이 됨

    public UserServiceInFunctionalWay(Predicate<User> validateUser, Consumer<User> writeToDB){
        this.validateUser = validateUser;
        this.writeToDB = writeToDB;
    }

    public void createUser(User user){ // <- 큰 틀의 알고리즘
        if(validateUser.test(user)){
            writeToDB.accept(user);
        }else{
            System.out.println("Cannot create user");
        }
    }
}
