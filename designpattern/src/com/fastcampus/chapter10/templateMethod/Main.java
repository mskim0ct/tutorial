package com.fastcampus.chapter10.templateMethod;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        User alice = User.builder(1, "alice")
                .with(builder ->{
                    builder.emailAddress = "alice@fastcampus.co.kr";
                    builder.isVerified = false;
                    builder.friendUserIds = Arrays.asList(101,102,103,104,105,106,107,108);
                }).build();

        //큰 틀인 AbstractUserService의 createUser 의 알고리즘은 변경되지 않지만, UserService 에서 제공하는 동작이 수행됨
        UserService userService = new UserService();
        userService.createUser(alice);
        //큰 틀인 AbstractUserService의 createUser 의 알고리즘은 변경되지 않지만, InternalUserService 에서 제공하는 동작이 수행됨
        InternalUserService internalUserService =new InternalUserService();
        internalUserService.createUser(alice);

        //즉, 동일한 createUser 라는 알고리즘에서  각 상황에 맞게, 구체적인 조건들은 다르게 제공할 수 있게 함.


        //아래는 함수형 프로그래밍을 사용하여 Template Design Pattern을 적용
        //함수형 프로그래밍을 통해, 클래스를 만들어 인스턴스를 생성하지 않고,
        //간략하게 lambda를 이용하는 방법.
        UserServiceInFunctionalWay userServiceInFunctionalWay = new UserServiceInFunctionalWay( // <- 세부 조건 작성
                user->{
                    System.out.println("Validating user "+ user.getName());
                    return user.getName() != null && user.getEmailAddress().isPresent();
                },
                user->{
                    System.out.println("Write user "+user.getName()+" to DB");
                }
        );
        userServiceInFunctionalWay.createUser(alice); // <- 큰 틀의 알고리즘은 변경되지 않음

    }
}
