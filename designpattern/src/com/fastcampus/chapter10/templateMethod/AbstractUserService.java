package com.fastcampus.chapter10.templateMethod;

public abstract class AbstractUserService {
    protected abstract boolean validateUser(User user);

    protected abstract void writeToDB(User user);

    //알고리즘의 뼈대 - Template
    public void createUser(User user){
        if(validateUser(user)){
            writeToDB(user);
        }else{
            System.out.println("Cannot create user");
        }
    }
}


