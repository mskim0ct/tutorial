package com.example.hateoas.client;

import com.example.hateoas.server.User;
import com.example.hateoas.server.UserEntityModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/*******************************************
 * RestTemplate 을 이용하여 REST API 호출하기
 * ******************************************/
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RestTemplateClientExample {

    private RestTemplate restTemplate;
    private final String url = "http://localhost:8080";

    @BeforeEach
    void setUp() {
        restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        //restTemplate = new RestTemplate();
    }

    /***POST***/
    //postForEntity
    //ResponseBody 와 연결되는 객체를 포함하는 ResponseEntity 반환
    @Order(0)
    @Test
    void postForEntityExample() {
        ResponseEntity<User> responseEntity = restTemplate.postForEntity(url+"/users", new User("hoho", Arrays.asList(1,2,3)), User.class);
        System.out.println("[POST] postForEntity status Code : "+responseEntity.getStatusCode());
    }
    //postForLocation
    //새로 생성된 리소스의 URL 반환
    @Order(1)
    @Test
    void postForLocationExample() {
        URI uri = restTemplate.postForLocation(url+"/users", new User("haha", Arrays.asList(1,2)), User.class);
        System.out.println("[POST] postForLocation Location : "+uri);
    }

    //postForObject
    //ResponseBody 와 연결되는 객체를 반환
    @Order(2)
    @Test
    void postForObjectExample() {
        User user = restTemplate.postForObject(url+"/users", new User("hehe", Arrays.asList(2,6,7)), User.class);
        System.out.println("[POST] postForObject object : " + user.toString());
    }

    /***GET***/
    //getForEntity
    //ResponseBody 와 연결되는 객체를 포함하는 ResponseEntity 반환
    @Test
    @Order(3)
    void getForEntityExample() {
        ResponseEntity<User> responseEntity = restTemplate.getForEntity(url+"/users/2", User.class);
        System.out.println("[GET] getForEntity status Code : "+responseEntity.getStatusCode());
    }
    //getForObject
    //ResponseBody 와 연결되는 개체를 반환
    @Test
    @Order(4)
    void getForObjectExample() {
        User result = restTemplate.getForObject(url+"/users/{id}", User.class, 2);
        System.out.println("[GET] getForObject object : "+result.toString());
    }

    /***PUT***/
    //PUT 메서드 실행하며, 반환타입은 void
    @Test
    @Order(5)
    void putExample() {
        System.out.println("=== Before PUT === ");
        getForObjectExample();
        restTemplate.put(url+"/users/{id}", new User("aliceDot", Arrays.asList(4,1,2)),1);

        System.out.println("\n === AFTER PUT === ");
        getForObjectExample();
    }

    /***PATCH***/
    //ResponseBody와 연결되는 결과 객체 반환
    @Test
    @Order(6)
    void patchForObjectExample() {
        System.out.println(" === BEFORE PATCH === ");
        HttpHeaders headers =new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity entity = new HttpEntity("{\"userFriendIds\":[1,2]}", headers);


        User user = restTemplate.patchForObject(URI.create(url+"/users/2"),
                entity, User.class);

        System.out.println(" === AFTER PATCH === ");
        System.out.println(user.toString());
    }

    /***DELETE***/
    @Test
    @Order(8)
    void deleteExample() {
        restTemplate.delete(url+"/users/{id}",3);
    }

    /***OPTION***/
    @Test
    @Order(7)
    void optionExample() {
        Set<HttpMethod> set = restTemplate.optionsForAllow(url+"/users/1");
        System.out.println("OPTION : "+set);
    }
}
