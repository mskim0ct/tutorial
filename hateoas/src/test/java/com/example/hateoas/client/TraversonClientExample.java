package com.example.hateoas.client;

import com.example.hateoas.server.User;
import com.example.hateoas.server.UserEntityModel;
import org.junit.jupiter.api.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.MediaType;

import java.net.URI;
import java.util.Objects;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TraversonClientExample {

    private Traverson traverson;
    @BeforeAll
    void setUp() {
         traverson = new Traverson(
                URI.create("http://localhost:8080/users/hal"),
                MediaTypes.HAL_JSON);

    }

    @Test
    void getResources() {
        CollectionModel<UserEntityModel> collectionModel =
                traverson.follow("self")
                                .toObject(new ParameterizedTypeReference<CollectionModel<UserEntityModel>>() {
                                });
        Assertions.assertEquals(2, collectionModel.getContent().size());
        System.out.println(collectionModel.getContent());
    }

    @Test
    void getResource() {
        UserEntityModel userEntityModel = traverson.follow("1")
                .toObject(UserEntityModel.class);
        Assertions.assertEquals("alice", userEntityModel.getName());

    }
}
