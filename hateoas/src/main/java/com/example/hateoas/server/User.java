package com.example.hateoas.server;

import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import java.util.List;

@Table(name = "userinfo")
@Entity
public class User{

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column
    @ElementCollection
    private List<Integer> userFriendIds;

    public User(String name, List<Integer> userFriendIds){
        this.name = name;
        this.userFriendIds = userFriendIds;
    }

    public User() {

    }
    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public String getName() {
        return name;
    }

    public List<Integer> getUserFriendIds() {
        return userFriendIds;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUserFriendIds(List<Integer> userFriendIds) {
        this.userFriendIds = userFriendIds;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", userFriendIds=" + userFriendIds +
                '}';
    }
}
