package com.example.hateoas.server;

import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class UserEntityModel extends RepresentationModel<UserEntityModel> {

    private String name;
    private List<Integer> userFriendIds;

    public UserEntityModel(){}

    public UserEntityModel(User user){
        this.name = user.getName();
        this.userFriendIds = user.getUserFriendIds();
    }

    public void setName(String name){
        this.name = name;
    }
    public void setUserFriendIds(List<Integer> userFriendIds){
        this.userFriendIds = userFriendIds;
    }
    public String getName(){
        return name;
    }
    public List<Integer> getUserFriendIds(){
        return userFriendIds;
    }

}
