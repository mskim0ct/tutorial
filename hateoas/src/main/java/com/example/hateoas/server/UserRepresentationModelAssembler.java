package com.example.hateoas.server;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

public class UserRepresentationModelAssembler extends RepresentationModelAssemblerSupport<User, UserEntityModel> {

    /**
     * Creates a new {@link RepresentationModelAssemblerSupport} using the given controller class and resource type.
     *
     * @param controllerClass must not be {@literal null}.
     * @param resourceType    must not be {@literal null}.
     */
    public UserRepresentationModelAssembler(Class controllerClass, Class resourceType) {
        super(controllerClass, resourceType);
    }


    @Override
    public UserEntityModel toModel(User user) {
        UserEntityModel userEntityModel = instantiateModel(user);
        userEntityModel.add(WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(UserController.class).retrieveHalUser(user.getId()))
                .withSelfRel());
       // UserEntityModel userEntityModel = createModelWithId(user.getId(), user);
        userEntityModel.setName(user.getName());
        userEntityModel.setUserFriendIds(user.getUserFriendIds());
        return userEntityModel;
    }

    @Override
    public CollectionModel<UserEntityModel> toCollectionModel(Iterable<? extends User> entities) {
        return super.toCollectionModel(entities);
    }
}
