package com.example.hateoas.server;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/users")
    public User createUser(@RequestBody User user, HttpServletResponse response) {
        User result = userRepository.save(user);
        response.setHeader(HttpHeaders.LOCATION,
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class).retrieveUser(result.getId())).toString());
        return user;
    }

    //    @GetMapping("/users/{id}")
//    public ResponseEntity<User> retrieveUsers(@PathVariable Long id){
//        User user = userRepository.findById(id).get();
//        return ResponseEntity.ok(user);
//    }
    @GetMapping("/users")
    public CollectionModel<UserEntityModel> retrieveUsers() {
        CollectionModel<UserEntityModel> userEntityModels = new UserRepresentationModelAssembler(
                UserController.class,
                UserEntityModel.class
        ).toCollectionModel(userRepository.findAll());

        return userEntityModels.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class)
                .retrieveUsers()).withSelfRel());
        //return userEntityModels.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class).retrieveUsers()).withRel("retrieveUsers"));
    }


    @GetMapping("/users/{id}")
    public User retrieveUser(@PathVariable Long id) {
        User user = userRepository.findById(id).get();
        return user;
    }


    @PutMapping("/users/{id}")
    public User putUser(@RequestBody User user, @PathVariable Long id) {
        user.setId(id);
        return userRepository.save(user);
    }

    @PatchMapping("/users/{id}")
    public User patchUser(@RequestBody User user, @PathVariable Long id) {
        User result = userRepository.getById(id);
        if (user.getUserFriendIds() != null) {
            result.setUserFriendIds(user.getUserFriendIds());
        }
        return userRepository.save(result);
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable Long id) {
        userRepository.delete(userRepository.getById(id));
    }


    /********************************************************************************/
    /********************************************************************************/

    //HAL 적용
    @GetMapping("/users/hal/{id}")
    public UserEntityModel retrieveHalUser(@PathVariable Long id) {
        return new UserRepresentationModelAssembler(
                UserController.class,
                UserEntityModel.class
        ).toModel(userRepository.findById(id).get());
    }

    //HAL 적용
    @GetMapping("/users/hal")
    public CollectionModel<UserEntityModel> retrieveHalUsers() {
        CollectionModel<UserEntityModel> userEntityModels = new UserRepresentationModelAssembler(
                UserController.class,
                UserEntityModel.class
        ).toCollectionModel(userRepository.findAll());

        return userEntityModels.add(
                    WebMvcLinkBuilder.linkTo(
                            WebMvcLinkBuilder.methodOn(UserController.class).retrieveHalUsers()
                    ).withSelfRel());


    }


}
