package com.example.hateoas;

import com.example.hateoas.server.User;
import com.example.hateoas.server.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class HateoasApplication {

    @Autowired
    private UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(HateoasApplication.class, args);

    }

    @EventListener(ApplicationReadyEvent.class)
    public void init(){
        System.out.println("======ApplicationReadyEvent========");
        List<User> users = Arrays.asList(new User("alice",Arrays.asList(1,2,3)), new User("bob",Arrays.asList(1,2)));
        userRepository.saveAll(users);
    }
}

