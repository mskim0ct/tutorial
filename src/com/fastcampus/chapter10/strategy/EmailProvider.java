package com.fastcampus.chapter10.strategy;

public interface EmailProvider {
    String getEmail(User user);
}
