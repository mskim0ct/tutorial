package com.fastcampus.chapter10.strategy;

public class VerifyYourEmailAddressEmailProvider implements EmailProvider{
    @Override
    public String getEmail(User user) {
        return "'Verify Your Email Address' email for "+user.getName();
    }
}
