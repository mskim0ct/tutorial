package com.fastcampus.chapter10.strategy;

public class EmailSender {
    private EmailProvider emailProvider;

    //실시간으로 전략을 변경할 수 있도록 setter
    public void setEmailProvider(EmailProvider emailProvider){
        this.emailProvider = emailProvider;
    }

    public void sendEmail(User user){
        String email = emailProvider.getEmail(user);
        System.out.println("Sending "+email);
    }
}
