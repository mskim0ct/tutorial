package com.fastcampus.chapter10.strategy;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        User user1 = User.builder(1,"Alice")
                .with(builder -> {
                    builder.emailAddress = "Alice@fastcampus.co.kr";
                    builder.isVerified = false;
                    builder.friendUserIds = Arrays.asList(201,202,203,204,205,206,207);
                }).build();
        User user2 = User.builder(2,"Bob")
                .with(builder -> {
                    builder.emailAddress = "Bob@fastcampus.co.kr";
                    builder.isVerified = true;
                    builder.friendUserIds = Arrays.asList(201,202,203);
                }).build();
        User user3 = User.builder(3,"Chalie")
                .with(builder -> {
                    builder.emailAddress = "Chalie@fastcampus.co.kr";
                    builder.isVerified = true;
                    builder.friendUserIds = Arrays.asList(201,202,203,204,205,206,207,12,1,4,5,8);
                }).build();


        List<User> users = Arrays.asList(user1,user2,user3);

        //전략 생성
        EmailSender emailSender = new EmailSender();
        EmailProvider verifyYourEmailAddressEmailProvider = new VerifyYourEmailAddressEmailProvider();
        EmailProvider makeMoreFriendsEmailProvider = new MakeMoreFriendsEmailProvider();

        //전략 변경
        emailSender.setEmailProvider(verifyYourEmailAddressEmailProvider);
        users.stream()
                .filter(user ->!user.isVerified())
                .forEach(emailSender::sendEmail); //동일한 sendEmail로 다른 전략을 적용할 수 있음

        //전략 변경
        emailSender.setEmailProvider(makeMoreFriendsEmailProvider);
        users.stream()
                .filter(User::isVerified)
                .filter(user-> user.getFriendUserIds().size() < 5)
                .forEach(emailSender::sendEmail); //동일한 sendEmail로 다른 전략을 적용할 수 있음

        //함수형프로그래밍 적용한 전략 변경
        emailSender.setEmailProvider(user -> "'Play With Friends' email for "+user.getName());
        users.stream()
                .filter(User::isVerified)
                .filter(user -> user.getFriendUserIds().size() >= 5)
                .forEach(emailSender::sendEmail);
    }

}
