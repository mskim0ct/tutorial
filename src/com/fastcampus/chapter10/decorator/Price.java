package com.fastcampus.chapter10.decorator;

public class Price {
    private final String price;

    public Price(String price){
        this.price = price;
    }

    public String getPrice(){
        return price;
    }
}
