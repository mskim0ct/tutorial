package com.fastcampus.chapter10.chanofresponsibility;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class Order {
    private Long id;
    private LocalDateTime createdAt;
    private long createdByUserId;
    private OrderStatus status;
    private BigDecimal amount;
    private List<OrderLine> orderLines;

    public enum OrderStatus {
        CREATED,
        ERROR,
        IN_PROGRESS,
        PROCESSED
    }

    public Order setId(Long id ){
        this.id = id;
        return this;
    }

    public Order setStatus(OrderStatus status){
        this.status = status;
        return this;
    }


    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Order setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public long getCreatedByUserId() {
        return createdByUserId;
    }

    public Order setCreatedByUserId(long createdByUserId) {
        this.createdByUserId = createdByUserId;
        return this;
    }


    public Long getId(){
        return id;
    }

    public OrderStatus getStatus(){
        return status;
    }

    public BigDecimal getAmount(){
        return amount;
    }

    public Order setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }


    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public Order setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
        return this;
    }

    @Override
    public String toString() {
        return "com.fastcampus.chapter10.chanofresponsibility.Order [id=" + id + ", " + (createdAt != null ? "createdAt=" + createdAt + ", " : "")
                + "createdByUserId=" + createdByUserId + ", " + (status != null ? "status=" + status + ", " : "")
                + (amount != null ? "amount=" + amount + ", " : "")
                + (orderLines != null ? "orderLines=" + orderLines : "") + "]";
    }
}
