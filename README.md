# Tutorial
개발 관련 공부하면서, 작성한 코드들을 올려놓는 프로젝트입니다.


### designpattern
- chain of responsibility
- strategy
- template method 
- decorator

### HATEOAS
- HATEOAS: server 측에서 설정하는 방법
- RestTemplate, Traverson: client에서 API 호출하는 방법 ( src/test/.../client 디렉토리 하위에 test 파일로 존재함 )
